// S19toROM  Converts S19 (srec) file to a C array, and puts it in a .h file
// ========
// Bugs:
//-------------------------------------------------------------------
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
//===================================================================
public class S19toROM
{
static int ndwrds;
static BufferedReader br;
static PrintWriter pw;
//-------------------------------------------------------------------
public S19toROM() {}
//-------------------------------------------------------------------
public static void main(String[] args)
{
String line;
try
	{
	if (args.length!=1) Error("Please specify an S19 (srec) file to process.\nUsage: java S19toROM filename");
	br=new BufferedReader(new InputStreamReader(new FileInputStream(new File(args[0]))));
  pw=new PrintWriter(new File("s19.h"));
	pw.println("static const unsigned short int rom[]={"); ndwrds=0;
	while ((line=br.readLine())!=null) Crunch(line);
	pw.println("};\n");
	pw.close(); br.close();
	System.out.println("Done. Result is in s19.h");
	}//try
catch (Exception ex) {System.out.println("Exception [main]: could not process file "+args[0]);}
}//main
//-------------------------------------------------------------------
static void Crunch(String s)
{
String shdr;
shdr=s.substring(0,2);
     if (shdr.equals("S1")) CrunchS1(s);
else if (shdr.equals("S0")) System.out.println("head");
else if (shdr.equals("S9")) System.out.println("tail");
else Error("Unknow S19 line header:"+s);     
}//Crunch
//-------------------------------------------------------------------
static void CrunchS1(String s1)
{
int len,adr,val[],n4,i,j,fix;
len=Integer.decode("0x"+s1.substring(2,4));
adr=Integer.decode("0x"+s1.substring(4,8));
val=new int[4];
n4=(len-2-1)/4;
for (i=0;i<n4;i++) 
  {
  //01 23 4567 89 ab cdef0123456789abcdef
  //S1 13 0410 00 20 0021002200230024002500260027BC
  for (j=0;j<4;j++) val[j]=Integer.decode("0x"+s1.substring(8+i*8+j*2,8+i*8+j*2+2));
  //---- Make sure 0x040D is always 0xFF ---------------
  fix=0x040D-adr; if ((fix>=0)&&(fix<=3)) val[fix]=0xFF;
  //----------------------------------------------------
  pw.print(String.format("0x%04X,0x%02X,0x%02X,0x%02X,0x%02X,  ",adr,val[0],val[1],val[2],val[3]));
  adr+=4; ndwrds++; if (ndwrds>3) {pw.println(); ndwrds=0;}
  }
if (((len-2-1)%4)!=0) Error("Cannot manage S19 records that are not multiples of dwrds (for now)");
}//CrunchS1
//-------------------------------------------------------------------
static void Error(String e) 
{
System.out.println("Error: "+e);
pw.println("Error: "+e);
try {pw.close(); br.close();} catch (IOException ioe) {ioe.printStackTrace();}
System.exit(0);
}//Error
//-------------------------------------------------------------------
}//class S19toROM
//===================================================================
