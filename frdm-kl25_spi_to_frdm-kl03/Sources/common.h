#ifndef __COMMON_H
#define __COMMON_H


// HARDWARE /////////////////////////////////////////////////////////
//-------------------------------------------------------------------
// B18 = nRGB_R
// B19 = nRGB_G
// D01 = nRGB_B


// INCLUDES /////////////////////////////////////////////////////////                 
//-------------------------------------------------------------------
#include "MKL25Z4.h"


// TYPEDEFS /////////////////////////////////////////////////////////
//-------------------------------------------------------------------
typedef unsigned char	      byte;
typedef unsigned short int	word;
typedef unsigned long int	  dwrd;
typedef char			          int8;
typedef short int	         int16;
typedef int		             int32;


// DEFINES //////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------
#define RGB_SCG                   SIM_SCGC5|=SIM_SCGC5_PORTB_MASK; SIM_SCGC5|=SIM_SCGC5_PORTD_MASK
#define RGB_MUX                   PORTB_PCR18=PORT_PCR_MUX(1); PORTB_PCR19=PORT_PCR_MUX(1); PORTD_PCR1 =PORT_PCR_MUX(1)
#define RGB_GPIO_outputs          GPIOB_PDDR|=(1<<18); GPIOB_PDDR|=(1<<19); GPIOD_PDDR|=(1<< 1)
#define RGB_R0                    GPIOB_PSOR=(1<<18)
#define RGB_R1                    GPIOB_PCOR=(1<<18)
#define RGB_G0                    GPIOB_PSOR=(1<<19)
#define RGB_G1                    GPIOB_PCOR=(1<<19)
#define RGB_B0                    GPIOD_PSOR=(1<< 1)
#define RGB_B1                    GPIOD_PCOR=(1<< 1)
//...................................................................
#define SPI_SCG                   SIM_SCGC4|=SIM_SCGC4_SPI0_MASK; SIM_SCGC5|=SIM_SCGC5_PORTC_MASK
#define SPI_MUX                   PORTC_PCR4 =PORT_PCR_MUX(1); PORTC_PCR5 =PORT_PCR_MUX(2); PORTC_PCR6 =PORT_PCR_MUX(2); PORTC_PCR7 =PORT_PCR_MUX(2)
#define SPI_GPIO_outputs          GPIOC_PDDR|=(1<< 4); GPIOC_PSOR=(1<< 4)
#define SPI_SS0                   GPIOC_PCOR=(1<< 4)
#define SPI_SS1                   GPIOC_PSOR=(1<< 4)
//...................................................................
#define DUART_SCG                 SIM_SCGC5|=SIM_SCGC5_PORTA_MASK
#define DUART_MUX_Tx              PORTA_PCR1 =PORT_PCR_MUX(2)
#define DUART_MUX_Rx              PORTA_PCR2 =PORT_PCR_MUX(2)
#define DUART_Init                UART0_Init
#define DUART_CharOut             UART0_CharOut
#define DUART_NibbOut             UART0_NibbOut
#define DUART_ByteOut             UART0_ByteOut
#define DUART_WordOut             UART0_WordOut
#define DUART_DWrdOut             UART0_DWrdOut
#define DUART_TextOut             UART0_TextOut
#define DUART_RDRF                UART0_RDRF
#define DUART_CharIn              UART0_CharIn
#define DUART_NibbIn              UART0_NibbIn
#define DUART_ByteIn              UART0_ByteIn
#define DUART_DbleOut             UART0_DbleOut


// GLOBAL VARIABLES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    


// FUNCTION HEADERS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------
void MCU_Init(void);
void PIT_Init(void);
void PIT_Delay(dwrd delay);                                          // delay in multiples of 100us
void RGB_Init(void);
void RGB(byte R,byte G,byte B);


#endif //__COMMON_H
