#ifndef __UART_H
#define __UART_H


// INCLUDES /////////////////////////////////////////////////////////                 
//-------------------------------------------------------------------
#include "common.h"


// DEFINES //////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------
#define RDRF                      0x20                               // for UART calls
#define TDRE                      0x80                               // for UART calls


// GLOBAL VARIABLES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    


// FUNCTION HEADERS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------  
void UART0_Init(word data);                                          // initializes the serial port
void UART0_CharOut(byte data);                                       // sends out a character
void UART0_NibbOut(byte data);                                       // sends out a nibble (hex value)
void UART0_ByteOut(byte data);                                       // sends out a byte (hex value)
void UART0_WordOut(word data);                                       // sends out a word (hex value)
void UART0_DWrdOut(dwrd data);                                       // sends out a double word (hex value)
void UART0_TextOut(char *data);                                      // sends out a string
byte UART0_RDRF(void);                                               // checks to see if a character is present
byte UART0_CharIn(void);                                             // reads a character
byte UART0_ByteIn(void);                                             // reads two characters and interprets them as a byte (hex)
void UART0_DbleOut(float data,byte d1,byte d2);                      // sends out double (float); d1=digits before decimal point, d2=after


#endif //__UART_H
