/*
 * [File Name]     main.c
 * [Platform]      Freescale FRDM-KL25Z and FRDM-KL03 Evaluation Board
 * [Project]       FRDM-KL25Z (SPI Master) controlling FRDM-KL03Z (SPI Slave) Bootloader, see ConvertS19.docx file
 *                 Change the name of the s19xxxx.h file to .s19.h, then in TeraTerm, so a (e)erase and (f)fullwrite.
 * [Version]       1.00
 * [Author]        Rod Borras / Mike Steffen
 * [Date]          07JAN2016
 * [Language]      'C'
 * [History]       1.00 - Original Release
 *
 * Copyright (c) 2016, NXP, Inc.
 * All rights reserved.
 *
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of Freescale Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// INCLUDES /////////////////////////////////////////////////////////
//-------------------------------------------------------------------
#include "common.h"
#include "uart.h"
#include "spi.h"


// DEFINES //////////////////////////////////////////////////////////
//-------------------------------------------------------------------


// GLOBAL CONSTANTS /////////////////////////////////////////////////
//-------------------------------------------------------------------
#include "s19.h"


// GLOBAL VARIABLES /////////////////////////////////////////////////
//-------------------------------------------------------------------
byte cin,cksum,M[100];
word lines;


// FUNCTION HEADERS /////////////////////////////////////////////////
//-------------------------------------------------------------------
void BootOut(char *data);
word BootCRC(char *data);
void Bootloader_Ping(void);
void Bootloader_GetProperty(byte prop);
void Bootloader_S19(void);
void Bootloader_Write(word adr,byte val0,byte val1,byte val2,byte val3);
void Bootloader_EraseUnsecure(void);
void Bootloader_FullWrite(void);
void S19_ReadName(void);
void S19_Program(void);
void S19_End(void);


//**** MAIN *********************************************************
//===================================================================
int main(void)
{
MCU_Init(); RGB_Init(); PIT_Init(); DUART_Init(13);
SPI_Init(0x16);
for (;;)
  {
  DUART_TextOut("SPI Bootloader v1.04: (p)ing, (g)et_property, (s)19_file, (w)rite, (e)rase_unsecure, (f)ullwrite\n\r");
  cin=DUART_CharIn();
  switch (cin)
    {
    case 'p': Bootloader_Ping();             break;
    case 'g': Bootloader_GetProperty(1);     break;
    case 's': Bootloader_S19();              break;
    case 'w': Bootloader_Write(0xBEAD,0xC9,0xA8,0x7C,0x0A); break;
    case 'f': Bootloader_FullWrite();        break;
    case 'e': Bootloader_EraseUnsecure();    break;
    default:  DUART_ByteOut(cin);            break;
    }
  DUART_TextOut("\n\r");
  }
return 0;
}
//===================================================================
//*******************************************************************


// FUNCTION BODIES //////////////////////////////////////////////////
//-------------------------------------------------------------------
void S19_ReadName(void)
{
byte len,c,i,ah,al;
word adr;
cksum=0;
len=DUART_ByteIn(); cksum+=len;
ah=DUART_ByteIn(); cksum+=ah; al=DUART_ByteIn(); cksum+=al; adr=ah*256+al;
DUART_TextOut("  - Filename=[");
for (i=0;i<len-2-1;i++) {c=DUART_ByteIn(); cksum+=c; DUART_CharOut(c);}
DUART_TextOut("] ");
c=DUART_ByteIn(); DUART_ByteOut(c); DUART_CharOut('='); DUART_ByteOut(~cksum);
if (c==(byte)(~cksum)) DUART_TextOut(" cksum ok"); else DUART_TextOut(" *** CHECKSUM ERROR ***");
DUART_TextOut("\n\r");
}
//-------------------------------------------------------------------
void S19_Program(void)
{
byte len,c,i,ah,al;
word adr;
cksum=0;
len=DUART_ByteIn(); cksum+=len;
ah=DUART_ByteIn(); cksum+=ah; al=DUART_ByteIn(); cksum+=al; adr=ah*256+al;
for (i=0;i<len-2-1;i++) {c=DUART_ByteIn(); cksum+=c;}
DUART_TextOut("  - Line 0x"); DUART_WordOut(lines); DUART_TextOut(":  ");
c=DUART_ByteIn(); DUART_ByteOut(c); DUART_CharOut('='); DUART_ByteOut(~cksum);
if (c==(byte)(~cksum)) DUART_TextOut(" cksum ok"); else DUART_TextOut(" *** CHECKSUM ERROR ***");
DUART_TextOut("\n\r");
}
//-------------------------------------------------------------------
void S19_End(void)
{
byte len,c,i;
cksum=0;
len=DUART_ByteIn(); cksum+=len;
for (i=0;i<len-1;i++) {c=DUART_ByteIn(); cksum+=c;}
DUART_TextOut("  - End of file:  ");
c=DUART_ByteIn(); DUART_ByteOut(c); DUART_CharOut('='); DUART_ByteOut(~cksum);
if (c==(byte)(~cksum)) DUART_TextOut(" cksum ok"); else DUART_TextOut(" *** CHECKSUM ERROR ***");
DUART_TextOut("\n\r");
}


//-------------------------------------------------------------------
void Bootloader_FullWrite(void)
{
word len,iw;
len=sizeof(rom);
for (iw=0;iw<len/2;iw+=5) Bootloader_Write(rom[iw+0],rom[iw+1],rom[iw+2],rom[iw+3],rom[iw+4]);
}
//-------------------------------------------------------------------
void BootOut(char *data)
{
byte spin,pos,spl,sph;
pos=0;
while (*data!=0x00)
  {
       if (*data=='[') SPI_SS0;
  else if (*data==']') SPI_SS1;
  else if (*data=='?') {do spin=SPI_CharShift(0x00); while (spin!=0x5A); DUART_ByteOut(spin); DUART_CharOut(' ');}
  else if (*data=='*') {spin=SPI_CharShift(0x00); DUART_ByteOut(spin); DUART_CharOut(' ');}
  else if (*data==' ') pos=0;
  else
    {
    if (pos==0) {pos++; sph=(*data)-48; if (sph>9) sph-=7;}
    else        {pos=0; spl=(*data)-48; if (spl>9) spl-=7; (void)SPI_CharShift(sph*16+spl);}
    }
  data++;
  }
}
//-------------------------------------------------------------------
word BootCRC(char *data)
{
byte pos,sph,spl,i;
word crc,tmp;
pos=0; crc=0;
while (*data!=0x00)
  {
       if (*data==' ') pos=0;
  else if (*data=='c') pos=0;
  else
    {
    if (pos==0) {pos++; sph=(*data)-48; if (sph>9) sph-=7;}
    else        {pos=0; spl=(*data)-48; if (spl>9) spl-=7; crc^=(sph*16+spl)<<8; for (i=0;i<8;++i) {tmp=crc<<1; if (crc&0x8000) tmp^=0x1021; crc=tmp;}}
    }
  data++;
  }
return crc;
/*
uint16_t crc16_update(const uint8_t * src, uint32_t lengthInBytes)
{
uint32_t crc = 0;
uint32_t j;
for (j=0; j < lengthInBytes; ++j)
{
uint32_t i;
uint32_t byte = src[j];
crc ^= byte << 8;


}
return crc;
}
*/
}
//-------------------------------------------------------------------
void Bootloader_Ping(void)
{
DUART_TextOut("  - Bootloader Ping\n\r");
BootOut("[ 5A A6 ?* * * * * * * * * ]");
}
//-------------------------------------------------------------------
void Bootloader_GetProperty(byte prop)
{
DUART_TextOut("  - Bootloader GetProperty 1\n\r");
BootOut("[ 5A A4 08 00 73 D4    07    00 00 01     01 00 00 00  ?* ?* * * * * * * * * * * * * * * * * 5A A1 ]");
}
//-------------------------------------------------------------------
void Bootloader_S19(void)
{
byte notdone,sin;
DUART_TextOut("  - Bootloader S19: drop your S19 file into TeraTerm...\n\r");
notdone=1; lines=0;
do
  {
  do sin=DUART_CharIn(); while (sin!='S');
  lines++; //DUART_WordOut(lines); DUART_TextOut("\n\r");
  sin=DUART_CharIn();
       if (sin=='0') S19_ReadName();
  else if (sin=='1') S19_Program();
  else if (sin=='9') {S19_End(); notdone=0;}
  } while (notdone);
}
//-------------------------------------------------------------------
void LoadM(char *data)
{
byte p;
p=0;
while (*data!=0x00) {M[p++]=*data; data++;}
M[p]=0;
}
//-------------------------------------------------------------------
byte word2hex(word val,byte shift)
{
byte res;
res=(val>>shift)&0x0F;
res+='0'; if (res>'9') res+=7;
return res;
}
//-------------------------------------------------------------------
void Bootloader_Write(word adr,byte val0,byte val1,byte val2,byte val3)
{
word cksum;
DUART_TextOut("\n\rWriting to 0x"); DUART_WordOut(adr); DUART_TextOut("\n\r");
LoadM("[ 5A A4 0C 00 cc cc 04 00 00 02 AA AA 00 00 04 00 00 00"); M[32]=word2hex(adr,4); M[33]=word2hex(adr,0); M[35]=word2hex(adr,12); M[36]=word2hex(adr,8); //DUART_TextOut(M); DUART_TextOut("\n\r");
cksum=BootCRC(M); //DUART_WordOut(cksum); DUART_TextOut("\n\r");
M[14]=word2hex(cksum,4); M[15]=word2hex(cksum,0); M[17]=word2hex(cksum,12); M[18]=word2hex(cksum,8); //DUART_TextOut(M); DUART_TextOut("\n\r");
BootOut(M); BootOut(" ?* ?* * * * * * * * * * * * * * * * * 5A A1");
LoadM("  5A A5 04 00 cc cc VV VV VV VV");
M[20]=word2hex(val0,4); M[21]=word2hex(val0,0);
M[23]=word2hex(val1,4); M[24]=word2hex(val1,0);
M[26]=word2hex(val2,4); M[27]=word2hex(val2,0);
M[29]=word2hex(val3,4); M[30]=word2hex(val3,0);
//DUART_TextOut(M); DUART_TextOut("\n\r");
cksum=BootCRC(M); //DUART_WordOut(cksum); DUART_TextOut("\n\r");
M[14]=word2hex(cksum,4); M[15]=word2hex(cksum,0); M[17]=word2hex(cksum,12); M[18]=word2hex(cksum,8); //DUART_TextOut(M); DUART_TextOut("\n\r");
BootOut(M); BootOut(" ?* ?* * * * * * * * * * * * * * * * * 5A A1 ]");

//cksum=BootCRC("5A A4 08 00 c c 07 00 00 01 01 00 00 00"); //73d4
//cksum=BootCRC("5A A4 0C 00 c c 0C 00 00 02 0A 00 00 00 01 00 00 00"); //678d
//cksum=BootCRC("5A A5 08 00 c c BE EF C9 A7 12 23 AB CD");             DUART_WordOut(cksum);                        //dffb
}
//-------------------------------------------------------------------
void Bootloader_EraseUnsecure(void)
{
DUART_TextOut("  - Bootloader Erase_Unsecure\n\r");
BootOut("[ 5A A4 04 00 F6 61    0D    00 00 00 ?*  ?*   * * * * * * * * * * * * * * * * 5A A1 ]");
}
