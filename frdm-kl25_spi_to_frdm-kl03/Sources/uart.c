// INCLUDES /////////////////////////////////////////////////////////                  
//-------------------------------------------------------------------
#include "uart.h"


// FUNCTION BODIES //////////////////////////////////////////////////                    
//-------------------------------------------------------------------
// UART0 FUNCTIONS //////////////////////////////////////////////////
//-------------------------------------------------------------------
void UART0_Init(word bd)
{
SIM_SCGC4|=SIM_SCGC4_UART0_MASK; DUART_SCG; DUART_MUX_Tx; DUART_MUX_Rx;
UART0_C2=0x00; UART0_BDH=bd>>8; UART0_BDL=bd&0xFF; UART0_C1=0x00; UART0_S2=0x00; UART0_C3=0x00; UART0_C2=0x0C;
}
void UART0_CharOut(byte data)   {while ((UART0_S1&TDRE)==0); UART0_D=data;}
void UART0_NibbOut(byte data)   {if (data>0x09) UART0_CharOut(data+0x37); else UART0_CharOut(data+0x30);}
void UART0_ByteOut(byte data)   {UART0_NibbOut(data>>4); UART0_NibbOut(data&0x0F);}
void UART0_WordOut(word data)   {UART0_ByteOut(data>>8); UART0_ByteOut(data&0xFF);}
void UART0_DWrdOut(dwrd data)   {UART0_WordOut(data>>16); UART0_CharOut('_'); UART0_WordOut(data&0xFFFF);}
void UART0_TextOut(char *data)  {while (*data!=0x00) {UART0_CharOut(*data); data++;}}
byte UART0_RDRF(void)           {if (UART0_S1&RDRF) return 1; else return 0;}
byte UART0_CharIn(void)         {if (UART0_S1&0x1F) UART0_S1=0x1F; while ((UART0_S1&RDRF)==0); return UART0_D;}
byte UART0_ByteIn(void)         {byte h,l; h=UART0_CharIn()-48; if (h>9) h-=7; l=UART0_CharIn()-48; if (l>9) l-=7; return (h&15)*16+(l&15);}
void UART0_DbleOut(float data,byte d1,byte d2)
{
byte tmp,showzero,digs;
double idiv;
showzero=0;
idiv=1.0; for (digs=0;digs<d1;digs++) idiv*=10.0;
if (data<0) {UART0_CharOut('-'); data=-data;} else UART0_CharOut('+');
for (digs=0;digs<=d1+d2;digs++)
  {
  tmp=data/idiv;
  if (tmp) {UART0_NibbOut(tmp); showzero=1;} else if (showzero) UART0_NibbOut(tmp); else UART0_CharOut(' ');
  data-=tmp*idiv; idiv/=10.0;
  if (digs==d1) UART0_CharOut('.');
  }
}
