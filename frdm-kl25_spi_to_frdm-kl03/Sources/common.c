// INCLUDES /////////////////////////////////////////////////////////                  
//-------------------------------------------------------------------
#include "common.h"


// FUNCTION BODIES //////////////////////////////////////////////////                    
//-------------------------------------------------------------------
void MCU_Init(void)
{
// Crucial
__disable_irq();                                                     // Disable interrupts
SIM_COPC=0x00;                                                       // Disable COP watchdog
// System Registers
SIM_CLKDIV1=0;                                                       // Core=Flash=24MHz
SIM_SOPT2=0x05000000;                                                // TPM Input Clock, UART0 Clock is MCGFLLCLK
// System clock initialization
MCG_C4|=0x80;                                                        // push to 24MHz clock
MCG_C1=0x04;                                                         // FEI mode, internal clock 32KHz
MCG_C2=0x80;                                                         // LOC reset enable, slow ref clock select
}
//-------------------------------------------------------------------
void PIT_Init(void)
{
SIM_SCGC6|=SIM_SCGC6_PIT_MASK;                                       // Enable PIT   module
PIT_MCR=0x00;                                                        // MDIS=0: enable timers
PIT_LDVAL0=0xFFFFFFFF;                                               // PIT0 use for measuring time intervals
PIT_LDVAL1=2400;                                                     // PIT1 Period=100us
PIT_TCTRL0=0x0001;                                                   // Enable PIT0, polling
PIT_TCTRL1=0x0001;                                                   // Enable PIT1, polling
}
//-------------------------------------------------------------------
void PIT_Delay(dwrd delay)
{dwrd delw; for (delw=0;delw<delay;delw++) {while (!PIT_TFLG1); PIT_TFLG1=1;}}
//-------------------------------------------------------------------
void RGB_Init(void)
{
RGB_SCG; RGB_MUX; RGB_GPIO_outputs;
RGB(0,0,0);
}
//-------------------------------------------------------------------
void RGB(byte R,byte G,byte B)
{
if (R) RGB_R1; else RGB_R0;
if (G) RGB_G1; else RGB_G0;
if (B) RGB_B1; else RGB_B0;
}

